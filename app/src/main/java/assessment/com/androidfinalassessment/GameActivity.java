package assessment.com.androidfinalassessment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;

public class GameActivity extends AppCompatActivity {

    TextView rangeTextView;
    TextView hintTextView;
    TextView numberOfGuessesTextView;
    Button enterButton;
    EditText guessEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        rangeTextView = (TextView) findViewById(R.id.rangeTextView);
        hintTextView = (TextView) findViewById(R.id.hintTextView);
        numberOfGuessesTextView = (TextView) findViewById(R.id.numberOfGuessesTextView);
        enterButton = (Button) findViewById(R.id.enterButton);
        guessEditText = (EditText) findViewById(R.id.guessEditText);

//        initialize()
//        ParseQuery<ParseObject>
//        getQuery()
//        findInBackground()
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
