package assessment.com.androidfinalassessment;

/**
 * Created by Sirjan Kafle on 3/16/16.
 * Utilities provides you with some useful functions
 */

public class Utilities {
    public static int randInt(int N) {
        // Returns a random number between 1 and N inclusive
        return (int) (Math.random() * N + 1);
    }

    public static float updateAverage(float average, int newVal, int n) {
        return (average * n + newVal) / (n + 1);
    }
}
